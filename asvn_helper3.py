'''
Created on 21.06.2010

@author: tgiesel

Ported to Python 3
 - ngrimm
'''

import sys
import os
import getpass
import html
import re
import urllib.parse as urlparse

repository_substitution_list = {
     "OM"     : "http://ber-rd2124:18080/svn/openmob",
     "PLAY"   : "http://ber-rd2124:18080/svn/playground",
     "TGTEST" : "file:///export/home/tgiesel/testrepo/testrepo"
    }

### -------------------------------------------------------------------------
class AsvnExternal:
    def __init__(self, keyword, unidentified, url, dir, comment):
        self.keyword = keyword
        self.unidentified = unidentified
        self.url = url
        self.dir = dir
        self.comment = comment
        #print 'keyword:', keyword, 'unidentified:', unidentified,
        #print 'url:', url, 'dir:', dir, 'comment:', comment

### -------------------------------------------------------------------------
class SvnExternal:
    """
    """
    def __init__(self, dir, url, full_url):
        self.dir = dir
        self.url = url              # may contain "^"
        self.full_url = full_url    # incl. "http://" etc.

### -------------------------------------------------------------------------
def to_unicode_or_bust(obj):
    """Try to convert obj to a unicode string, whatever it is. If it is a 
    unicode string already, return id directly.
    """
    if not isinstance(obj, str):
        try:
            obj = obj.decode('utf-8')
        except Exception as _:
            try:
                obj = obj.decode('latin_1')
            except Exception as _:
                obj = obj.decode('ascii', 'ignore')
    return obj

### -------------------------------------------------------------------------
def escconv(str):
    """Convert the input to an UTF-8 string and escape it to be usable in HTML.
    Return a byte string."""
    ret = html.escape(to_unicode_or_bust(str).encode("utf-8"))
    return ret

### -------------------------------------------------------------------------
def strconv(str):
    """Convert the input to an UTF-8 string, return a byte string.""" 
    ret = to_unicode_or_bust(str).encode("utf-8")
    print(type(ret))
    return ret

### -------------------------------------------------------------------------
log_message = "<no message yet>"
def get_log_message():
    """Callback function for pysvn.
    """
    global log_message
    return True, log_message

### -------------------------------------------------------------------------
def simple_login_prompt(realm, username, may_save):
    """Beware: If this callback fails, subversion will simply give up,
    so test it carefully!"""
    try:
        sys.stderr.write('Authentication realm: %s\n' % realm)
        if username == None or username == "":
            sys.stderr.write('Username: ')
            sys.stderr.flush()
            username = sys.stdin.readline().strip()
        password = getpass.getpass("Password for '%s': " % username)
    
        # What's the meaning of the _input_ parameter "may_save"?
        sys.stderr.write('Do you want to store this password (in plain text) on your disk (N|y)? ')
        sys.stderr.flush()
        response = sys.stdin.readline().strip()
        if response == "y" or response == "Y":
            may_save = 1
        else:
            may_save = 0
    except Exception as e:
        sys.stderr.write('simple_login_prompt: %s\n' % str(e))
        return (False, username, '', False)
    return (True, username, password, may_save)

### -------------------------------------------------------------------------
def get_repository_substitution_list(indend_by = "    "):
    """
    Return the repository substitution list in a human readable form
    """
    global repository_substitution_list
    text = ""
    for key in repository_substitution_list:
        text = text + indend_by + key
        text = text + (10 - len(key)) * " " 
        text = text + "=> " + repository_substitution_list[key] + "\n"
    return text

### -------------------------------------------------------------------------
def make_repository_substitution(orig_repo):
    """
    Check if a repository name can be replaced by a path from the 
    substitution table. If yes, do it. Return the resulting
    repository path or the original value.
    """
    global repository_substitution_list
    if repository_substitution_list.get(orig_repo) != None:
        return repository_substitution_list[orig_repo]
    else:
        return orig_repo
    
### -------------------------------------------------------------------------
def check_highest_dir(client, root_url, dir):
    """
    If ROOT_URL/URL ends with '*', replace it with the matching entry with the
    highest revision number which can be found in that directory.
    If such a substitution fails, return None. Otherwise return the URL
    of the directory found.
    If no substitution has to be done, return the original DIR.
    """
    dir_components = dir.split('/')
    if len(dir_components) == 0:
        return dir
    asterisk_pos = dir_components[-1].find('*')
    if asterisk_pos < 0:
        return dir

    if asterisk_pos != len(dir_components[-1]) - 1:
        errput("Wildcard must be the last character in path specifier")
        return None

    # if we are here, the last directory component contains an '*'
    # let's use "/tags/appl/1006*" for the examples
    
    # re-create the dir by its components, but w/o last component
    base_dir = ""
    for p in dir_components[:-1]:
        if p != '':
            base_dir += '/' + p
    
    # now base_dir is e.g. "/tags/appl"
    # it may become to something like "/tags/appl/1006"
    prefix_dir = base_dir + '/' + dir_components[-1][:asterisk_pos] 

    # do not url_add_component here, it will remove a trailing '/' which is 
    # needed to avoid ambiguous wildcards like "make/*" => "make" and "make_it" 
    base_url   = root_url + base_dir
    prefix_url = root_url + prefix_dir
    
    # get list of files in that directory
    try:
        file_list = client.list(base_url, recurse=False)
    except pysvn.ClientError as e:
        errput(str(e))
        return None
    
    # filter the list according to the prefix
    file_list = list(filter(lambda a: a[0]["path"].find(prefix_url) == 0, file_list))
    
    if len(file_list) == 0:
        errput("No match for wildcard '" + dir + "'")
        return None
    
    
    file_list.sort(key=lambda x: x[0]["created_rev"].number)
    return file_list[-1][0]["repos_path"]


### -------------------------------------------------------------------------
def url_canonicalize(url):
    """Sanitize URL. 
    Remove duplicated and trailing "/".
    """
    url_components = urlparse.urlparse(url)
    path_components = url_components[2].split('/')
    # re-create the path by its components
    new_path = ""
    for p in path_components:
        if p != '':
            new_path += '/' + p
    ret = urlparse.urlunparse((url_components[0], 
                               url_components[1],
                               new_path,
                               url_components[3],
                               url_components[4],
                               url_components[5]))    
    return ret

### -------------------------------------------------------------------------
def url_add_component(url, add):
    """Add a component to PATH, make sure not to create double slashes
    between them.
    """
    return url_canonicalize(url_canonicalize(url) + '/' + add)

### -------------------------------------------------------------------------
def errput(msg, file=None, only_a_warning=False):
    '''
    Log MSG to stderr and to FILE is it is not None.
    MSG is a string or something that can be transformed to a string.
    Decorate it with ERROR or WARNING determined by ONLY_A_WARNING.  
    '''
    if only_a_warning:
       log("*** WARNING: " + str(msg), file)
    else:
       log("*** ERROR: " + str(msg), file)

### -------------------------------------------------------------------------
def log(text, file=None):
    '''
    Log TEXT to stderr and to FILE is it is not None.
    TEXT is a string or something that can be transformed to a string.  
    '''  
    sys.stderr.write(str(text) + '\n')
    if file != None:
        file.write(str(text) + '\n')

### -------------------------------------------------------------------------
def log_verbose(verbose, text, file=None):
    '''
    If verbose is True, log TEXT to stderr and to FILE is it is not None. 
    TEXT is a string or something that can be transformed to a string.  
    '''
    if (verbose):
        log(text, file)

### -------------------------------------------------------------------------
# def get_property(self, url, propname):
#     """Get a property PROPNAME for URL from the repository. This is done
#     non-recursively. Return None if it doesn't have this property."""

#     rev = core.svn_opt_revision_t()
#     rev.kind = core.svn_opt_revision_head

#     try:
#         prop = client.svn_client_propget2(propname, url, rev, rev, 0, self.ctx)
#     except core.SubversionException as e:
#         #pass;
#         #if e.apr_err != core.SVN_ERR_RA_ILLEGAL_URL:
#         errput("Failed to check '" + url + "': " + str(e))
#         return None

#     if (prop and prop[url]):
#         return prop[url]
#     else:
#         return None

### -------------------------------------------------------------------------
def parse_externals(externals_txt, root_url, url):
    """Parse externals
    Return a dictionary with entries <dir> : SvnExternal.
    If the element has no externals or they are empty, return an empty 
    dictionary. 
    If another error occurs, return None. 
    """
    
    externals_txt_list = externals_txt.splitlines()
    
    result = {}
    for line in externals_txt_list:
        line = line.strip()
        if len(line):
            components = line.split()
            if len(components) != 2:
                errput("Line '" + line + 
                       "' contains an unknown/unimplemented external specifier")
                return None
            url = components[0]
            dir = components[1]
            if url[0] == '^':
                # replace "^" with root_url
                full_url = url_add_component(root_url, url[1:])
                result[dir] = SvnExternal(dir, url, full_url)
            else:
                result[dir] = SvnExternal(dir, url, url)

    return result

### -------------------------------------------------------------------------
def get_status(client, path, report_errors=True):
    """Get the status of a svn tree. Return a dictionary for all 'interesting'
    entries: path_name : status, both are human readable text. Return None if
    an error occured.
    The content may be filtered or cleaned up in some way, that's why it's a
    better idea to use this function instead of calling client.status 
    directly.
    """
    try:
        changes = client.status(path, get_all=False, ignore=True)
    except pysvn.ClientError as e:
        if report_errors:
            errput(str(e))
        return None
    dict = {}
    for f in changes:
        text_changed = False
        prop_changed = False
        if f.text_status != pysvn.wc_status_kind.external and \
           f.text_status != pysvn.wc_status_kind.normal:
            text_changed = True
        if f.prop_status != pysvn.wc_status_kind.none and \
           f.prop_status != pysvn.wc_status_kind.normal:
            prop_changed = True
        
        if text_changed or prop_changed:  
            if text_changed:
                dict[f.path] = str(f.text_status)
            elif prop_changed:
                dict[f.path] = 'properties ' + str(f.prop_status)
    return dict

### -------------------------------------------------------------------------
def get_entry_info(client, url, verbose=False, report_errors=True):
    """Get information about an svn entry.
    Print an error message and return None if it failed, return the info
    otherwise.
    """ 
    if verbose:
        print("Checking '" + url + "'...")
    try:
        info = client.info2(url, recurse = False)
    except pysvn.ClientError as e:
        if report_errors:
            errput('get svn info: ' + str(e))
        return None
    return info[0][1]

def ask(msg):
    """Ask the user if he wants to do something. MSG contains a question.
    Return True for yes and False for no.
    """
    print(msg)
    while True:
        print("'y'|'n': ",)
        response = sys.stdin.readline().strip()
        if response.upper() == "Y" or response.upper() == 'YES':
            return True
        elif response.upper() == "N" or response.upper() == 'NO':
            return False

### -------------------------------------------------------------------------
def check_create_dir(client, url, verbose=False, yes=False, dry_run=False):
    """Check if a directory exists, create it otherwise.
    If the directory does not exist, ask the user if it shall be created.
    Return True if the directory exists or was created, return False if it is
    missing.
    """
    info = get_entry_info(client, url, verbose, report_errors=False)
    if info != None:
        return True    
    if yes:
        response = True
    else:
        response = ask("Path '" + url + "' does not exist yet.\n" + \
              "Please check the URL carefully. Do you want me to create it?")
    if response == False:
        return False
    if dry_run:
        print(" (dry-run) mkdir --parents " + url)
    else:
        try:
            client.mkdir(url, "New directory '" + url + "'", True)
            if verbose:
                print("Directory " + url + " created.")
        except pysvn.ClientError as e:
            errput(str(e))
            return False
    return True

### -------------------------------------------------------------------------
def get_dir_list(client, root_url, url):
    """
    NOT TESTED
    If the URL is empty, return an empty list. 
    If another error occurs, return None. 
    """
    try:
        list = client.list(url, recurse=True)
    except pysvn.ClientError as e:
        errput(str(e))
        return None
    
    #print list
    sys.exit()

### -------------------------------------------------------------------------
class SvnHelperDiff:
    """
    """
    def __init__(self, file_a, file_b, diff):
        self.file_a = file_a
        self.file_b = file_b
        self.diff   = diff      # A list of lines starting with one of (" +-@")

### -------------------------------------------------------------------------
def parse_diff(diff_txt):
    """Parse a diff in unified format.
    Return a list with entries of type SvnHelperDiff.
    """
    diff_list = []
    diff_lines = diff_txt.splitlines()
    current_file_name_a = None
    current_file_name_b = None
    current_diff = []
    for line in diff_lines:
        if current_file_name_a and current_file_name_b:
            if line[:5] == "Index":
                # a new file is going to start, save the previous one
                diff_list.append(SvnHelperDiff(current_file_name_a,
                                               current_file_name_b,
                                               current_diff))
                current_file_name_a = None
                current_file_name_b = None
                current_diff = []
            else:
                current_diff.append(line)

        if not current_file_name_a or not current_file_name_b:
            if line[:3] == "---":                
                current_file_name_a = line[3:].strip()
    
            elif line[:3] == "+++":
                current_file_name_b = line[3:].strip()
            else:
                pass
                # ignore line, it's between two files

    # check if we have information left
    if len(current_diff):
        diff_list.append(SvnHelperDiff(current_file_name_a,
                                       current_file_name_b,
                                       current_diff))
    return diff_list

### -------------------------------------------------------------------------
def split_asvn_external_line(line, do_strip=True):
    '''Split an asvn:external line into its parts. It may have one of the
    following parts:
        <spaces only>
        '#'<comment>
        <url> <dir> ['#'<comment>]
        'remove' <dir>
    If DO_STRIP is False, leading and trailing spaces and trailing line breaks
    are kept in the components. The returned components have exactly the same 
    number of characters as the input LINE in this case.
    This feature is needed for the syntax coloring in asvn_externals.py. 
    In this case, if the caller concatenates 
        keyword + unidentified + url + dir + comment
    he gets a string which is identical with LINE.
    '''
    #print 'line: ', line
    unidentified = ''
    keyword = ''
    url = ''
    dir = ''
    comment = ''
    rest = ''
    # parse comment
    rest = line
    # parse empty lines
    if rest.strip() == '':
        comment = rest
        rest    = ''
    parts = re.match(r'([^#]*)(#.*$)', rest, re.DOTALL)
    if parts:
        rest    = parts.group(1)
        comment = parts.group(2)
        #print 'rest:', rest, 'comment:', comment
    # try to parse 'remove' <dir>
    parts = re.match(r'(\s*remove\s+)(\S+\s*$)', rest, re.DOTALL)
    if parts:
        keyword = parts.group(1)
        dir     = parts.group(2)
        rest    = ''
        #print 'keyword:', keyword, 'dir:', dir
    # try to parse <url> <dir>
    parts = re.match(r'(\s*\S+\s+)(\S+\s*$)', rest, re.DOTALL)
    if parts:
        url  = parts.group(1)
        dir  = parts.group(2)
        rest = ''
        #print 'url:', url, 'dir:', dir
    if rest:
        unidentified = rest
    if keyword + unidentified + url + dir + comment != line:
        print('Failed to parse:', line)      # refer to comment
    if do_strip:
        return AsvnExternal(keyword.strip(), unidentified.strip(), 
                        url.strip(), dir.strip(), comment.strip())
    else:
        return AsvnExternal(keyword, unidentified, url, dir, comment)
        

### -------------------------------------------------------------------------
def parse_asvn_externals(default_externals, custom_externals):
    '''Parse and merge DEFAULT_EXTERNALS and CUSTOM_EXTERNALS.
    Return plain svn:externals as a string and the number of errors. 
    '''
    n_errors = 0
    default_lines = default_externals.splitlines()
    default_list = []
    for line in default_lines:
        default_list.append(split_asvn_external_line(line))

    custom_lines = custom_externals.splitlines()
    custom_list = []
    for line in custom_lines:
        custom_list.append(split_asvn_external_line(line))

    # remove all custom entries from the default list which aim at the same
    # directory.
    for custom_entry in custom_list:
        for default_entry in default_list:
            if not custom_entry.unidentified and custom_entry.dir and \
                    custom_entry.dir == default_entry.dir:
                default_list.remove(default_entry)

    str = ''
    for entry in custom_list + default_list:
        if entry.unidentified:
            str += 'Error in line "%s"\n' % entry.unidentified
            n_errors += 1
        elif entry.keyword == 'remove':
            # it has been removed from default_list already
            pass 
        else:
            str += entry.url + ' ' + entry.dir + '\n'

    return str, n_errors

#!/bin/env python3.10
'''
Ported to Python 3 with PySimpleGui
 - ngrimm

based on version from tgiesel
'''

import PySimpleGUI as psg
import tkinter as tk
import argparse
import asvn_helper3
import os, stat, shutil, sys
import subprocess
import threading
import xml.etree.ElementTree as ET

psg.theme("Gray Gray Gray")

def rmtree_force(path):
    os.chmod(path, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
    for dirname, dirnames, filenames in os.walk(path):
        for f in filenames:
            full = os.path.join(dirname, f)
            os.chmod(full, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
        for f in dirnames:
            full = os.path.join(dirname, f)
            os.chmod(full, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
    # and then remove it
    shutil.rmtree(path)

def captured_cmd(cmd, input_trigger=[": "], inp=lambda: input(""), out=lambda s: print(s, end="")):
    p = subprocess.Popen(cmd, bufsize=0, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.PIPE)

    last = ""
    last_unicode = bytes()
    rc = None
    while True:
        p.poll()
        if p.returncode != None:
            rc = p.returncode
            break

        raw = p.stdout.read(1)
        try:
            output = raw.decode()
        except UnicodeDecodeError:
            last_unicode += raw
            continue

        if len(last_unicode) > 0:
            output = last_unicode.decode() + output
            last_unicode = bytes()

        out(f"{output}")

        if input_trigger != None:
            if any([output in it for it in input_trigger]):
                last += output
            else:
                last = ""
            
            if any([last.endswith(it) for it in input_trigger]):
                try:
                    inp_s = inp()
                    p.stdin.write((inp_s + "\n").encode())
                except Exception as e:
                    pass

    output = p.stdout.read().decode()
    out(f"{output}")
    return rc

class UpdateThread(threading.Thread):
    def __init__(self, path, callback):
        super().__init__()
        self.ret = None
        self.path = path
        self.callback = callback

    def run(self):
        self.ret = captured_cmd(["svn", "update", "--force-interactive", self.path], input_trigger=None, out=self.callback)

class AsvnExternalsWindow:
    def __init__(self):
        self.path = None
        self.busy = False
        self.root_url = None

        menubar = [
            ["&File", ["&Open working copy", "&Load default externals", "&Set externals, update", "E&xit"]],
            # TODO:    v
            # ["&Edit", ["&Find"]]
        ]

        editor_column = [
            [psg.Text("Custom externals")],
            [psg.Multiline(size=(70, 20), key="-CUSTOM-", enable_events=True, write_only=True, rstrip=False)],
            [psg.Text("Default externals")],
            [psg.Multiline(size=(70, 20), key="-DEFAULT-", enable_events=True, write_only=True, rstrip=False)],
        ]

        preview_column = [
            [psg.Text("Preview")],
            [psg.Multiline(size=(70, 42), disabled=True, key="-PREVIEW-")]
        ]

        layout = [
            [psg.MenuBar(menubar)],
            [psg.Column(editor_column), psg.VSeparator(), psg.Column(preview_column)]
        ]

        self.window = psg.Window("asvn externals", layout, grab_anywhere=True, resizable=True, finalize=True)

    def run(self):
        while True:
            event, values = self.window.read()
            # print(event, '\n', values)

            if self.busy:
                pass # ignore events
            elif event == "Exit" or event == psg.WIN_CLOSED:
                self.exit()
                break
            elif event == "-CUSTOM-":
                self.text_highlight("-CUSTOM-")
                self.update_preview()
            elif event == '-DEFAULT-':
                self.text_highlight("-DEFAULT-")
                self.update_preview()
            elif event == "-EXTERNALS FILE-":
                self.load_defaults_from_file(values["-EXTERNALS FILE-"])
            elif event == "Open working copy":
                path = psg.popup_get_folder("Select a working directory")
                if path:
                    self.select_dir(path)
            elif event == "Load default externals":
                p = subprocess.run(["svn", "propget", "asvn:default_externals_prev_file", self.path], capture_output=True)
                prev_externals_file = p.stdout.decode("utf-8").strip()

                new_externals_file = psg.popup_get_file("Select a externals file", initial_folder=os.path.dirname(prev_externals_file))
                self.load_defaults_from_file(new_externals_file)
                p = subprocess.run(["svn", "propset", "asvn:default_externals_prev_file", new_externals_file, self.path])
            elif event == "Set externals, update":
                self.save_and_update()

        self.window.close()

    def exit(self):
        # ensure consistent state or issue a warning if mid-update
        # should the update ever be done in the background without deactivating the main window
        pass

    def load_defaults_from_file(self, file):
        with open(file, "r") as f:
            self.window["-DEFAULT-"].update(f.read())
        self.text_highlight("-DEFAULT-")
        self.update_preview()

    def select_dir(self, path, externals_checked=False):
        self.path = path
        self.root_url = self.get_root_url()

        p = subprocess.run(["svn", "propget", "svn:externals", self.path], capture_output=True)
        raw_externals = p.stdout.decode("utf-8").strip()
        p = subprocess.run(["svn", "propget", "asvn:custom_externals", self.path], capture_output=True)
        custom_externals = p.stdout.decode("utf-8").strip()
        p = subprocess.run(["svn", "propget", "asvn:default_externals", self.path], capture_output=True)
        default_externals = p.stdout.decode("utf-8").strip()

        # if no externals, ask if you want to create them now
        # if only raw is set, set default to raw

        if not externals_checked and raw_externals == '' and default_externals == '' and custom_externals == '':
            psg.popup_error('%s has no externals.\n\n' % self.path +
                          'This is okay if you want to create them on a ' + 
                          'new directory now. Otherwise you may have ' + 
                          'selected a wrong directory.',
                          title='No externals found')
        
        if default_externals == '' and custom_externals == '' and raw_externals != '':
            # migrate asvn:externals
            default_externals = raw_externals
        
        self.window["-DEFAULT-"].update(default_externals)
        self.text_highlight("-DEFAULT-")
        self.window["-CUSTOM-"].update(custom_externals)
        self.text_highlight("-CUSTOM-")
        self.update_preview()

    def save_and_update(self):
        if not self.path:
            psg.popup_error("No working copy opened.", title="Update error")
            return

        if self.window["-DEFAULT-"].get() == "" and self.window["-CUSTOM-"].get() == "":
            psg.popup_error("No default nor custom externals defined", title="Update error")
            return

        n_errors = self.update_preview()
        if n_errors:
            psg.popup_error(f"Found {n_errors} in the externals.", title="Update error")
            return

        # TODO: log clear

        p = subprocess.run(["svn", "propset", "asvn:default_externals", self.window["-DEFAULT-"].get(), self.path])
        p = subprocess.run(["svn", "propset", "asvn:custom_externals", self.window["-CUSTOM-"].get(), self.path])
        p = subprocess.run(["svn", "propset", "svn:externals", self.window["-PREVIEW-"].get(), self.path])

        externals = asvn_helper3.parse_externals(self.window["-PREVIEW-"].get(), self.root_url, self.path)

        if externals == None:
            return

        ret = self.check_working_copy(externals)

        if ret == None:
            return

        dirs_to_delete, externals_to_add, externals_to_update = ret

        if not self.show_summary(dirs_to_delete, externals_to_add, externals_to_update):
            return
        self.execute(dirs_to_delete, externals_to_add, externals_to_update)
    
    def check_working_copy(self, externals):
        dirs_to_delete = []
        externals_to_add = {}
        externals_to_update = {}

        dir_entries_to_check = os.listdir(self.path)
        while len(dir_entries_to_check) != 0:
            name = dir_entries_to_check.pop(0).replace('\\', '/')
            
            if os.path.basename(name) == '.svn':
                continue

            fullpath = os.path.normpath(os.path.join(self.path, name))
            p = subprocess.run(["svn", "info", fullpath], capture_output=True)
            info = p.stdout.decode("utf-8").strip()
            err = p.stderr.decode("utf-8").strip()
            if err:
                info = None

            if name in externals:
                if info != None:
                    externals_to_update[name] = externals[name]
                else:
                    res = psg.popup_yes_no(f"'{name}' is a private directory but now there is an external for it.\nDo you want to remove it?", title="Update confirm")
                    if res == None:
                        return None
                    elif res == "Yes":
                        dirs_to_delete.append(name)
                    externals_to_add[name] = externals[name]
            elif info != None:
                res = psg.popup_yes_no(f"'{name}' is under version control but there is no externals for it anymore.\nDo you want to remove it?", title="Update confirm")
                if res == None:
                    return None
                elif res == "Yes":
                    dirs_to_delete.append(name)
            else:
                # this is a normal file system entry and may contain externals
                dir = os.path.normpath(os.path.join(self.path, name))
                if os.path.isdir(dir):
                    dir_list = os.listdir(dir)
                    for new_entry in dir_list:
                        dir_entries_to_check.append(os.path.join(name, new_entry))
                else:
                    # TODO: log
                    pass

        for key, value in externals.items():
            if key not in externals_to_update and key not in externals_to_add:
                externals_to_add[key] = value
                
        return dirs_to_delete, externals_to_add, externals_to_update

    
    def show_summary(self, dirs_to_delete, externals_to_add, externals_to_update):
        to_delete = "\n".join([str(d) for d in dirs_to_delete])
        to_add = "\n".join([str(v.dir) for k, v in externals_to_add.items()])
        to_update = "\n".join([str(v.dir) for k, v in externals_to_update.items()])

        layout = [
            [psg.Text("to delete")],
            [psg.Multiline(disabled=True, default_text=to_delete, size=(50, 10))],
            [psg.Text("to add")],
            [psg.Multiline(disabled=True, default_text=to_add, size=(50, 10))],
            [psg.Text("to update")],
            [psg.Multiline(disabled=True, default_text=to_update, size=(50, 10))],
            [psg.Ok()],
            [psg.Cancel()]
        ]
        
        window = psg.Window("summary", layout, grab_anywhere=True, resizable=True)

        while True:
            event, values = window.read()

            if event == "Ok":
                window.close()
                return True
            elif event == "Cancel" or event == "Exit" or event == psg.WIN_CLOSED:
                window.close()
                return False

    def execute(self, dirs_to_delete, externals_to_add, externals_to_update):
        self.busy = True
        # self.window.disable() # doesn't work on linux with Tkinter
        ok = True
        
        try:
            for dir in dirs_to_delete:
                path = os.path.join(self.path, dir)
                rmtree_force(path)
        except Exception as e:
            self.busy = False
            ok = False


        if ok:
            layout = [
                [psg.Multiline(key="-OUTPUT-", size=(50, 40))],
                [psg.Button("Done", key="-DONE-")]
            ]

            self.upd_window = psg.Window("update", layout, resizable=True, grab_anywhere=True, finalize=True)

            thread = UpdateThread(self.path, lambda s: self.upd_window["-OUTPUT-"].print(s, end=""))
            thread.start()

            while True:
                event, values = self.upd_window.read()

                if event == "-DONE-" and thread.ret != None:
                    break

            self.upd_window.close()

            if thread.ret != 0:
                psg.popup_error(f"An error occured while updating.\n", title="Update error")
                ok = False

        self.busy = False
        return True

    def get_root_url(self):
        p = subprocess.run(["svn", "--xml", "info", self.path], capture_output=True)
        s = p.stdout.decode("utf-8")
        x = ET.fromstring(s)

        return x.find("./entry/repository/root").text

    def text_highlight(self, key):
        widget = self.window[key].widget
        widget.tag_config("keyword", foreground="purple")
        widget.tag_config("unidentified", foreground="red")
        widget.tag_config("url", foreground="darkblue")
        widget.tag_config("dir", foreground="darkgreen")
        widget.tag_config("comment", foreground="darkgray")

        text = widget.get("1.0", tk.END).split("\n")
        for i, line in enumerate(text):
            external = asvn_helper3.split_asvn_external_line(line, do_strip=False)

            offset = 0
            if external.keyword != "":
                end = offset + len(external.keyword)
                widget.tag_add("keyword", f"{i+1}.{offset}", f"{i+1}.{end}")
                offset = end
            if external.unidentified != "":
                end = offset + len(external.unidentified)
                widget.tag_add("unindentified", f"{i+1}.{offset}", f"{i+1}.{end}")
                offset = end
            if external.url != "":
                end = offset + len(external.url)
                widget.tag_add("url", f"{i+1}.{offset}", f"{i+1}.{end}")
                offset = end
            if external.dir != "":
                end = offset + len(external.dir)
                widget.tag_add("dir", f"{i+1}.{offset}", f"{i+1}.{end}")
                offset = end
            if external.comment != "":
                end = offset + len(external.comment)
                widget.tag_add("comment", f"{i+1}.{offset}", f"{i+1}.{end}")
                offset = end

    def update_preview(self):
        raw_externals, errors = asvn_helper3.parse_asvn_externals(self.window["-DEFAULT-"].get(), self.window["-CUSTOM-"].get())
        self.window["-PREVIEW-"].update(raw_externals)
        self.text_highlight("-PREVIEW-")
        return errors


def test_externals(path):
    p = subprocess.run(["svn", "propget", "svn:externals", path], capture_output=True)
    raw_externals = p.stdout.decode("utf-8").strip()
    p = subprocess.run(["svn", "propget", "asvn:custom_externals", path], capture_output=True)
    custom_externals = p.stdout.decode("utf-8").strip()
    p = subprocess.run(["svn", "propget", "asvn:default_externals", path], capture_output=True)
    default_externals = p.stdout.decode("utf-8").strip()

    return not (raw_externals == '' and default_externals == '' and custom_externals == '')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Tool for working with externals")
    parser.add_argument("path", help="path to the working copy", nargs="?", default=".")
    args = parser.parse_args()

    path = os.path.abspath(args.path)

    if not test_externals(path):
        choice = psg.popup_yes_no(f"This directory '{path}' doesn't have any externals.\n"
                + "Do you want to create them here?\n"
                + "Otherwise this will search the parent directories.", title="No externals found.")
        
        if choice == None:
            sys.exit(1)

        if choice == "No":
            orig_path = path

            while True:
                path = os.path.abspath(os.path.join(path, ".."))

                if test_externals(path):
                    choice = psg.popup_yes_no(f"The parent directory '{path}' has externals.\n"
                            + "Do you want to open asvn_externals.py here?\n"
                            + "Otherwise this will continue searching.", title="Possible parent dir")
                    if choice == "Yes":
                        break

                if os.path.ismount(path):
                    psg.popup_error(f"Stopping search at filesystem boundary '{path}'.", title="End of search")
                    sys.exit(1)

    w = AsvnExternalsWindow()
    w.select_dir(path, externals_checked=True)
    w.run()
